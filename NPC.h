#ifndef NPC_H_
#define NPC_H_
#include "SDL2Common.h"
#include "Sprite.h"
// Forward declarations
// improve compile time.
class Vector2f;
class Animation;
class Game;  
class NPC : public Sprite
{
private:
	// Animation state
	int state;
	// Sprite information
	static const int SPRITE_HEIGHT = 64;
	static const int SPRITE_WIDTH = 32;
	Game* game;

public:
	NPC();
	~NPC();
	// Player Animation states
	enum NPCState { LEFT = 0, RIGHT, UP, DOWN, IDLE };
	// Overriding virtual parents
	void init(SDL_Renderer* renderer);
	void update(float timeDeltaInSeconds);
	// Update ai
	void ai();
	// Overriding pure virtual 
	int getCurrentAnimationState();
	void setGame(Game* game);

};
#endif