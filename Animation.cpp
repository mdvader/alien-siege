#include "Animation.h"
#include "SDL2Common.h"
#include <stdio.h>
#include <stdexcept>


Animation::Animation()
{
	// Should be set by init - use frames
	// to check for init not being called.
	maxFrames = 0;
	frames = nullptr;
	frameTimeMax = 0.4f; // Default to 2.5fps
	//set current animation frame to first frame.
	currentFrame = 0;
	//zero frame time accumulator
	accumulator = 0.0f;
}
void Animation::init(int noOfFrames,
	const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
	int row, int col)
{
	// set frame count.
	maxFrames = noOfFrames;
	// allocate frame array
	frames = new SDL_Rect[maxFrames];
	//Setup animation frames
	for (int i = 0; i < maxFrames; i++)
	{
		if (row == -1)
		{
			frames[i].x = (i * SPRITE_WIDTH); //ith col.
			frames[i].y = (col * SPRITE_HEIGHT); //col row.
		}
		else
		{
			if (col == -1)
			{
				frames[i].x = (row * SPRITE_WIDTH); //ith col.
				frames[i].y = (i * SPRITE_HEIGHT); //col row.
			}
			else
			{
				
				throw std::runtime_error("Bad parameters to init");
			}
		}
		frames[i].w = SPRITE_WIDTH;
		frames[i].h = SPRITE_HEIGHT;
	}
}


Animation::~Animation()
{
	
	delete[] frames;
	
	frames = nullptr;
}


SDL_Rect* Animation::getCurrentFrame()
{
	return &frames[currentFrame];
}

void Animation::update(float timeDeltaInSeconds)
{
	
	accumulator += timeDeltaInSeconds;
	// Update check
	if (accumulator > frameTimeMax)
	{
		currentFrame++;
		accumulator = 0.0f;
		if (currentFrame >= maxFrames)
		{
			currentFrame = 0;
		}
	}
}