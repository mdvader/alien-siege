#ifndef GAME_H_
#define GAME_H_
#include "SDL2Common.h"
class NPC;

class Game
{
private:
	// Declare window and renderer objects
	SDL_Window* gameWindow;
	SDL_Renderer* gameRenderer;
	// Background texture
	SDL_Texture* backgroundTexture;
	//Declare Player
	Player* player;
	// Window control
	bool quit;
	// Keyboard
	const Uint8* keyStates;
	// Game loop methods.
	void processInputs();
	void update(float timeDelta);
	void draw();
	NPC* npc;

public:
	// Constructor
	Game();
	~Game();
	// Methods
	void init();
	void runGameLoop();
	Player* getPlayer();

	// Public attributes
	static const int WINDOW_WIDTH = 800;
	static const int WINDOW_HEIGHT = 600;
};




#endif