#ifndef VECTOR2F_H_
#define VECTOR2F_H_
#include "SDL2Common.h"
class Vector2f
{
private:
	float x; ///< x coordinate
	float y;  ///< y coordinate
public:
	// Constructors
	Vector2f();
	Vector2f(float x, float y);
	Vector2f(Vector2f* other);
	// Destructor
	~Vector2f();
	void zero();
	void normalise();
	float length();

	
	void setX(float x);
	float getX(void);
	void setY(float y);
	float getY(void);
	void add(Vector2f* other);
	void scale(float scalar);

	/// Add vector
	void add(Vector2f* other);
	/// Subtract vector
	void sub(Vector2f* other);
};
#endif