#include "Game.h"
#include "TextureUtils.h"
#include <stdio.h>
#include <stdlib.h>
#include "playership.h"
#include <iostream>
#include "NPC.h"





Game::Game()
{
	gameWindow = nullptr;
	gameRenderer = nullptr;
	backgroundTexture = nullptr;
	player = nullptr;
	npc = nullptr;
    keyStates = nullptr;
	quit = false;
}
void Game::init()
{
	gameWindow = SDL_CreateWindow(
		"Alien Siege", // Window title
		SDL_WINDOWPOS_UNDEFINED, // X position
		SDL_WINDOWPOS_UNDEFINED, // Y position
		WINDOW_WIDTH, // width
		WINDOW_HEIGHT, // height
		SDL_WINDOW_SHOWN // Window flags
	);
	if (gameWindow != nullptr)
	{
		
		gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);
		if (gameRenderer == nullptr)
		{
			throw std::runtime_error(
				"SDL could not create renderer\n"
			);
		}
	}
	else
	{
		
		throw std::runtime_error("SDL could not create Window\n");
	}
	// Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);
	// Create background texture from file, optimised for renderer
	backgroundTexture
		= createTextureFromFile("assets/images/background.png",
			gameRenderer);
	if (backgroundTexture == nullptr)
		throw std::runtime_error("Background image not found\n");
	//setup player
	player = new Player();
	player->init(gameRenderer);

	npc = new NPC();
	npc->init(gameRenderer);
	npc->setGame(this);

}


Game::~Game()
{
	//Clean up
	delete player;
	player = nullptr;
	delete npc;
	npc = nullptr;
	SDL_DestroyTexture(backgroundTexture);
	backgroundTexture = nullptr;
	SDL_DestroyRenderer(gameRenderer);
	gameRenderer = nullptr;
	SDL_DestroyWindow(gameWindow);
	gameWindow = nullptr;
}
void Game::draw()
{
	// 1. Clear the screen
	SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
	// Colour provided as red, green, blue and alpha
	//(transparency) values (ie. RGBA)
	SDL_RenderClear(gameRenderer);
	// 2. Draw the scene
	SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);
	player->draw(gameRenderer);
	npc->draw(gameRenderer);
	// 3. Present the current frame to the screen
	SDL_RenderPresent(gameRenderer);
}

void Game::update(float timeDelta)
{
	player->update(timeDelta);
	npc->update(timeDelta);
}


void Game::processInputs()
{
	SDL_Event event;
	// Handle input
	if (SDL_PollEvent(&event)) // test for events
	{
		switch (event.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
			// Key pressed event
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				quit = true;
				break;
			}
			break;
			// Key released event
		case SDL_KEYUP:
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				
				break;
			}
			break;
		default:
			
			break;
		}
	}
	// Process Inputs - for player
	player->processInput(keyStates);
}
void Game::runGameLoop()
{
	// Timing variables
	unsigned int currentTimeIndex;
	unsigned int timeDelta;
	float timeDeltaInSeconds;
	unsigned int prevTimeIndex;
	// initialise preTimeIndex
	prevTimeIndex = SDL_GetTicks();
	// Game loop
	while (!quit) // while quit is not true
	{
		
		//- https://gafferongames.com/post/fix_your_timestep/
		currentTimeIndex = SDL_GetTicks();
		//time in milliseconds
		timeDelta = currentTimeIndex - prevTimeIndex;
		timeDeltaInSeconds = timeDelta * 0.001;
		// Store current time index
		// into prevTimeIndex for next frame
		prevTimeIndex = currentTimeIndex;
		// Process inputs
		processInputs();
		// Update Game Objects
		update(timeDeltaInSeconds);
		//Draw here.
		draw();
	}
}
Player* Game::getPlayer()
{
	return player;
}