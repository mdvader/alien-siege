#include "playership.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include <stdexcept>
#include "Sprite.h"


Player::Player() : Sprite()
{
	state = IDLE;
	speed = 50.0f;
	targetRectangle.w = SPRITE_WIDTH;
	targetRectangle.h = SPRITE_HEIGHT;
}


Player::~Player()
{
}

void Player::init(SDL_Renderer* renderer)
{
	//path string
	string path("playership.png");
	//postion
	Vector2f position(200.0f, 200.0f);
	// Call sprite constructor
	// 5 animations max
	Sprite::init(renderer, path, 5, &position);
	// Setup the animation structure
	animations[LEFT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
	animations[RIGHT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
	animations[UP]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
	animations[DOWN]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
	animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
	for (int i = 0; i < maxAnimations; i++)
	{
		animations[i]->setMaxFrameTime(0.2f);
	}
}







void Player::processInput(const Uint8* keyStates)
{
	// Process Player Input
	//Input - Keys
	float verticalInput = 0.0f;
	float horizontalInput = 0.0f;
	// No keys held down = No animation
	state = IDLE;

	
	if (keyStates[SDL_SCANCODE_UP])
	{
		verticalInput = -0.0025f;
		state = UP;

	}
	else if (keyStates[SDL_SCANCODE_DOWN])
	{
		verticalInput = 0.0025f;
		state = DOWN;
	}
	else
	{
		verticalInput = 0.0f;
	}

	if (keyStates[SDL_SCANCODE_LEFT])
	{
		horizontalInput = -0.0025f;
		state = LEFT;
	}
	else if (keyStates[SDL_SCANCODE_RIGHT])
	{
		horizontalInput = 0.0025f;
		state = RIGHT;
	}
	else
	{
		horizontalInput = 0.0f;
	}
	// Calculate player velocity.
	velocity->setX(horizontalInput);
	velocity->setY(verticalInput);
	velocity->normalise();
	velocity->scale(speed);


}
int Player::getCurrentAnimationState()
{
	return state;
}