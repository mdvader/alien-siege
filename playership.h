#ifndef PLAYER_H_
#define PLAYER_H_
#include "SDL2Common.h"
#include "Animation.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Sprite.h"
class Vector2f;
class Animation;


class Player : public Sprite
{
private:
	// Animation state
	int state;
	// Sprite information
	static const int SPRITE_HEIGHT = 64;
	static const int SPRITE_WIDTH = 32;
public:
	Player();
	~Player();
	// Player Animation states
	enum PlayerState { LEFT = 0, RIGHT, UP, DOWN, IDLE };
	void init(SDL_Renderer* renderer);
	void processInput(const Uint8* keyStates);
	int getCurrentAnimationState();
};


#endif