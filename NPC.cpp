#include "NPC.h"
#include "playership.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include <stdexcept>
#include "Sprite.h"
#include <stdio.h>


NPC::NPC() : Sprite()
{
	state = IDLE;
	speed = 50.0f;
	targetRectangle.w = SPRITE_WIDTH;
	targetRectangle.h = SPRITE_HEIGHT;
}

NPC::~NPC()
{
}

void NPC::init(SDL_Renderer* renderer)
{
	//path string
	string path("assets/images/undeadking.png");
	//postion
	Vector2f position(300.0f, 300.0f);
	// Call sprite constructor
	Sprite::init(renderer, path, 5, &position);
	// Setup the animation structure
	animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
	animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
	animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
	animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
	animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
	for (int i = 0; i < maxAnimations; i++)
	{
		animations[i]->setMaxFrameTime(0.4f);
	}
}

int NPC::getCurrentAnimationState()
{
	return state;
}

void NPC::ai()
{
	// get player
	Player* player = game->getPlayer();
	// get distance to player
	// copy the player position
	Vector2f vectorToPlayer(player->getPosition());
	// subtract our position to get a vector to the player
	vectorToPlayer.sub(position);
	float distance = vectorToPlayer.length();
	// if player 'in range' stop and fire
	if (distance < maxRange)
	{
		// fire
		// fire(vectorToPlayer);
	}
	else
	{
		
		state = IDLE;
		if (velocity->getY() > 0.1f)
			state = DOWN;
		else
			if (velocity->getY() < -0.1f)
				state = UP;
		
		if (velocity->getX() > 0.1f)
			state = RIGHT;
		else
			if (velocity->getX() < -0.1f)
				state = LEFT;
		
	}
}
void NPC::setGame(Game* game)
{
	this->game = game;
}
void NPC::update(float dt)
{
	ai();
	Sprite::update(dt);
}