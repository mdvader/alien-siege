#include <stdlib.h>
#include <stdio.h>
#include "SDL2Common.h"
#include "Game.h"
#include "playership.h"
#include "TextureUtils.h"
#include "Animation.h"
const int SDL_OK = 0;


int main(int argc, char* args[])
{
	
	int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);
	if (sdl_status != SDL_OK)
	{
		//SDL did not initialise, report and
		//error and exit.
		printf("Error - SDL Initialisation Failed\n");
		exit(1);
	}
	// Create and run game
	Game* game = new Game();
	game->init();
	game->runGameLoop();
	delete game;
	SDL_Quit();
	exit(0);
}